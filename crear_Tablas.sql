DROP TABLE IF EXISTS ActualizacionLimiteCredito;
DROP TABLE IF EXISTS Comisiones;
DROP TABLE IF EXISTS AsientoContable;
DROP TABLE IF EXISTS Facturas;

SOURCE tabla_limiteCredito.sql
SOURCE tabla_facturas.sql
SOURCE tabla_comisiones.sql
SOURCE tabla_asientoContable.sql
