CREATE TABLE Facturas (
    CodigoCliente INT,
    BaseImponible NUMERIC(15,2),
    IVA NUMERIC(15,2),
    Total NUMERIC(15,2),
    CodigoFactura INT AUTO_INCREMENT,

    PRIMARY KEY (CodigoFactura),
    FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
);
