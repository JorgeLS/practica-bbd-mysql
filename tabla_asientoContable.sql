CREATE TABLE AsientoContable (
    CodigoCliente INT,
    DEBE NUMERIC(15,2),
    HABER NUMERIC(15,2),

    FOREIGN KEY (CodigoCliente) REFERENCES Clientes(CodigoCliente)
);
