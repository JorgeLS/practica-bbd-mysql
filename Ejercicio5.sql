DROP PROCEDURE IF EXISTS MuestraInfoCliente;

DELIMITER //

CREATE PROCEDURE MuestraInfoCliente (IN cliente INT)

  BEGIN

    /* Si se pone 0 en el parametro se muestran todos los clientes */
    IF cliente = 0 THEN
        SELECT CodigoCliente, NombreCliente, Ciudad, CONCAT(LimiteCredito, ' €') AS 'Limite de Credito', COUNT(*) AS 'Num Pedidos', CONCAT(SUM(Cantidad * PrecioUnidad), ' €') AS 'Total Comprado'
        FROM Clientes NATURAL LEFT JOIN Pedidos NATURAL LEFT JOIN DetallePedidos
        GROUP BY Clientes.CodigoCliente;
    ELSEIF ( SELECT CodigoCliente FROM Clientes WHERE CodigoCliente = cliente) > 0 THEN
        SELECT CodigoCliente, NombreCliente, Ciudad, CONCAT(LimiteCredito, ' €') AS 'Limite de Credito', COUNT(*) AS 'Num Pedidos', CONCAT(SUM(Cantidad * PrecioUnidad), ' €') AS 'Total Comprado'
        FROM Clientes NATURAL LEFT JOIN Pedidos NATURAL LEFT JOIN DetallePedidos
        WHERE Clientes.CodigoCliente = cliente GROUP BY Clientes.CodigoCliente;
    ELSE
        SELECT cliente AS 'Cliente', 'No hay un Cliente con ese Número' AS 'ERROR';
    END IF;

  END; //

DELIMITER ;

/* Ejemplo */
CALL MuestraInfoCliente(0);
CALL MuestraInfoCliente(1);
CALL MuestraInfoCliente(2);
