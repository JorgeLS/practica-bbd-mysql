CREATE TABLE Comisiones (
    CodigoEmpleado INT,
    Comision NUMERIC(15,2),
    CodigoFactura INT,

    FOREIGN KEY (CodigoEmpleado) REFERENCES Empleados(CodigoEmpleado),
    FOREIGN KEY (CodigoFactura) REFERENCES Facturas(CodigoFactura)
);
