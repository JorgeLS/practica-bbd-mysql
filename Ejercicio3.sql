/* Borramos el procedimiento por si ya existe */
DROP PROCEDURE IF EXISTS Facturar;

/* Borramos y creamos la tabla */
DROP TABLE IF EXISTS Comisiones;
DROP TABLE IF EXISTS Facturas;

SOURCE tabla_facturas.sql;
SOURCE tabla_comisiones.sql;

/* Empezamos con el procedimiento llamado Facturar */
DELIMITER //

CREATE PROCEDURE Facturar ()

  BEGIN

    /* Declaramos las variables */
    DECLARE i INT;
    DECLARE final INT;
    DECLARE cliente INT;
    DECLARE factura INT;
    DECLARE empleado INT;
    DECLARE BaseImponible INT;
    DECLARE IVA INT;
    DECLARE comision INT;
    DECLARE Total INT;

    /* Completamos las variables que son estaticas e iniciamos la i */
    SET final = ( SELECT Pedidos.CodigoPedido FROM Pedidos ORDER BY Pedidos.CodigoPedido DESC LIMIT 1 );
    SET IVA = 21;
    SET i = 1;
    SET factura = 0;

    /* Empezamos con el bucle para rellenar la tabla de Facturas */
    llamada: LOOP

      /* Iniciamos un if para añadir todos las columnas */

      IF ( SELECT CodigoPedido FROM Pedidos WHERE CodigoPedido = i ) > 0 THEN
        SET cliente = ( SELECT CodigoCliente FROM Pedidos WHERE CodigoPedido = i );
        SET BaseImponible = ( SELECT SUM(PrecioUnidad * Cantidad) FROM DetallePedidos WHERE CodigoPedido = i );
        SET Total = BaseImponible + BaseImponible * ( IVA / 100 );

        INSERT INTO Facturas (CodigoCliente, BaseImponible, IVA, Total) VALUES (
            cliente,
            BaseImponible,
            BaseImponible * ( IVA / 100 ),
            Total
        );

        SET factura = factura + 1;

        SET empleado = (SELECT CodigoEmpleado From Clientes JOIN Empleados ON CodigoEmpleado = CodigoEmpleadoRepVentas WHERE CodigoCliente = cliente);
        SET comision = Total * .05;

        INSERT INTO Comisiones VALUES (
            empleado,
            comision,
            factura
        );
      END IF;

  SET i = i + 1;

  IF i <= final
    THEN ITERATE llamada;
  END IF;

  LEAVE llamada;
  END LOOP llamada;

  END; //

DELIMITER ;
