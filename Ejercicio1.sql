DROP PROCEDURE IF EXISTS Almacenado;

/* Actualizamos la tabla */
DROP TABLE IF EXISTS ActualizacionLimiteCredito;
SOURCE tabla_limiteCredito.sql;

/* Creamos el procedimiento */
DELIMITER //

CREATE PROCEDURE Almacenado ()

  BEGIN

    /*
      Ibamos a meter en una varaible el limiteCredito pero después de varios intentos no lo
      conseguimos, ya que si declaraba una variable con DECIMAL se distorsiona el resultado querido.
      Te dejamos los que pondriamos.
    */
    DECLARE i INT;
    DECLARE final INT;
    /* DECLARE limiteCredito DECIMAL(15, 2); */

    /* Declaramos el principio y el final del bucle */
    SET i = 1;
    SET final = (SELECT CodigoCliente FROM Pedidos ORDER BY CodigoCliente DESC LIMIT 1);

    llamada: LOOP

    IF
        (SELECT CodigoCliente FROM Clientes NATURAL JOIN Pedidos WHERE CodigoCliente = i AND
            (FechaPedido like "%2009%" OR FechaPedido like "%2008%" OR FechaPedido like "%2010%")
            GROUP BY CodigoCliente) > 0 THEN

        /*
        SET limiteCredito = (SELECT LimiteCredito FROM Clientes WHERE CodigoCliente = i);

        SET limiteCredito = limiteCredito + limiteCredito * .15;
        */

        INSERT INTO ActualizacionLimiteCredito VALUES (
            CURDATE(),
            i,
            /* limiteCredito */
            (SELECT LimiteCredito FROM Clientes WHERE CodigoCliente = i) +
            (SELECT LimiteCredito FROM Clientes WHERE CodigoCliente = i) * .15
        );

    END IF;

    SET i = i + 1;

    IF i <= final
        THEN ITERATE llamada;
    END IF;

    LEAVE llamada;
    END LOOP llamada;

  END; //

DELIMITER ;

CALL almacenado();

/*
		WHERE Clientes.CodigoCliente = Pedidos.CodigoCliente AND (Pedidos.FechaPedido like "%2009%" OR Pedidos.FechaPedido like "%2008%" OR Pedidos.FechaPedido like "%2010%");



UPDATE Clientes
		SET Clientes.LimiteCredito = Clientes.LimiteCredito + (Clientes.LimiteCredito*Incremento/100) 
		WHERE Clientes.CodigoCliente = CC AND  (Pedidos.FechaPedido like "%2009%" /*OR Pedidos.FechaPedido like "%2008%" OR Pedidos.FechaPedido like "%2010%");	*/
