/* Crea una funcion que te diga el mes */
DROP FUNCTION IF EXISTS UltimaFactura;

DELIMITER //

CREATE FUNCTION UltimaFactura()

  /* Variable que va ha retornar */
  RETURNS CHAR(50)

  BEGIN

    /* Variable para ser retornada */
    DECLARE codigo CHAR(4);
    DECLARE s CHAR(30);

    IF ( SELECT Count(Facturas.CodigoFactura) FROM Facturas ) = 0 THEN SET codigo = '1';
    ELSE SET codigo = ( SELECT Facturas.CodigoFactura FROM Facturas ORDER BY Facturas.CodigoFactura DESC LIMIT 1 );
    END IF;

    IF codigo = '1' THEN SET s = 'No hay nada. ERROR: ';
    ELSE
        SET s = 'La ultima factura es: ';
    END IF;

    /* Lo que devuelve */
    RETURN CONCAT (s,' ',codigo);

  END //

DELIMITER ;
