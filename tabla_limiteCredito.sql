CREATE TABLE ActualizacionLimiteCredito (
	Fecha DATE NOT NULL,
	CodigoCliente INT,
	Incremento DECIMAL(15, 2),

	CONSTRAINT FK_CLIENTES_ActualizacionLimiteCredito FOREIGN KEY(CodigoCliente) REFERENCES Clientes(CodigoCliente)
);
