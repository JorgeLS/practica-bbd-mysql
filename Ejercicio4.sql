DROP PROCEDURE IF EXISTS GenerarContabilidad;

DROP TABLE IF EXISTS AsientoContable;
SOURCE tabla_asientoContable.sql;

/* Empezamos con el procedimiento llamado Facturar */
DELIMITER //

CREATE PROCEDURE GenerarContabilidad ()

  BEGIN

    DECLARE i INT;
    DECLARE final INT;
    DECLARE cliente INT;
    DECLARE MAX_cliente INT;

    SET MAX_cliente = (SELECT CodigoCliente FROM Facturas ORDER BY CodigoCliente DESC LIMIT 1);
    SET final = MAX_cliente;
    SET i = 1;

    /* Empezamos con el bucle para rellenar la tabla AsientoContabilidad */
    llamada: LOOP

    IF ( SELECT CodigoCliente FROM Facturas WHERE CodigoCliente = i GROUP BY CodigoCliente ) > 0 THEN

        /* Sacamos el cliente para hacer el Deber */
        SET cliente = ( SELECT CodigoCliente FROM Facturas WHERE CodigoCliente = i GROUP BY CodigoCliente );

        INSERT INTO AsientoContable VALUES (
            cliente,
            (SELECT SUM(Total) FROM Facturas WHERE CodigoCliente = i GROUP BY CodigoCliente),
            0
        );

    END IF;

    SET i = i + 1;

    IF i <= final
        THEN ITERATE llamada;
    END IF;

    LEAVE llamada;
    END LOOP llamada;

  END; //

DELIMITER ;
